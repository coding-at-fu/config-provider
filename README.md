# ConfigProvider
Pyhton Konfigurationsklasse.

## Einbindung in andere Projekte

Dieses Projekt ist selbst als python package Strukturiert und kann daher einfach
als git submodule direkt in andere Projekte eingebunden werden.

```bash
git submodule add git@git.imp.fu-berlin.de:coding-at-fu/config-provider.git config_provider
```

## Verwendung
Wurde das Projekt wie oben stehend eingebunden kann das Modul wie folgt verwendet werden:

```python
import config_provider
import yaml

def load_config_from_yaml_file(path):
  with open(path) as f:
    yaml_string = f.read()
    return_dict = yaml.load(yaml_string)
  return return_dict

config = config_provider.ConfigProvider(
  load_function = {
    'function': load_config_from_yaml_file,
    'kwargs': {'path': '/path/to.yml'}})

def do_whatever():
  config = config_provider.ConfigProvider.getInstance()
  print (config.dict().get('config_key'))
  # wird foo ausgeben wenn config_key den Wert foo hat.

def reconfigure():
  config = config_provider.ConfigProvider.getInstance()
  config.reload()
  # Die Konfiguration wurde neu eingelesen.
```
