class ConfigProvider:

  _instance = None
  _configs = {}

  @classmethod
  def getInstance(cls, name = None):
    if name is None:
      return ConfigProvider._instance
    else:
      return ConfigProvider._configs.get(name)

  def __init__(self, load_function, name = None):
    self._conf_dict = load_function['function'](
      *load_function.get('args', []), **load_function.get('kwargs',{}))
    self._load_function = load_function
    if name is None:
      ConfigProvider._instance = self
    else:
      ConfigProvider._configs[name] = self

  @property
  def dict(self):
    return self._conf_dict

  def reload(self):
    self._conf_dict = self._load_function['function'](
      *self._load_function.get('args'), **self._load_function.get('kwargs'))