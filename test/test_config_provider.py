import unittest
import config_provider

class TestConfigProvider(unittest.TestCase):

  def setUp(self):
    self.dummy_config = {
      'A': 'a',
      'B': 'b'}
    def read_dummy_conf(self, **kwargs):
      conf_dict = dict(self.dummy_config, **kwargs)
      return conf_dict
    self.read_function = read_dummy_conf
    self.config = config_provider.ConfigProvider(
      load_function = {
        'function': self.read_function, 'args': [self],
        'kwargs': {'salt': 'pepper'}})

  def test_init(self):
    config = config_provider.ConfigProvider(
      load_function = {
        'function': self.read_function, 'args': [self],
        'kwargs': {'foo': 'bar'}})
    self.assertEqual({'foo': 'bar', 'A': 'a', 'B': 'b'}, config.dict)

  def test_reload(self):
    old_dict = dict(self.config.dict)
    self.dummy_config = {'A': 'a1', 'B': 'b'}
    expected_dict = {'A': 'a1', 'B': 'b', 'salt': 'pepper'}
    with self.subTest(
        'before Reload: self.config.dict = ' + str(self.config.dict)
        + ' old_dict = ' + str(old_dict) + ' self.dummy_config = '
        + str(self.dummy_config)):
      self.assertEqual(
        old_dict, self.config.dict, 'old_dict musst equal self.config.dict')
      self.assertNotEqual(
        self.dummy_config, self.config.dict,
        'self.dummy_config musst not equal self.config.dict')
    self.config.reload()
    with self.subTest('after Reload: self.config.dict = ' + str(self.config.dict)
        + ' old_dict = ' + str(old_dict) + ' self.dummy_config = '
        + str(self.dummy_config)):
      self.assertEqual(
        expected_dict, self.config.dict,
        'expected_dict musst equal self.config.dict')
      self.assertNotEqual(
        old_dict, self.config.dict, 'old_dict musst not equal self.config.dict')

  def test_get_instance(self):
    self.assertEqual(config_provider.ConfigProvider.getInstance(), self.config)

  def test_multiple_instances(self):
    def get_constant_dict():
      return {'foo': 'bar'}
    config_provider.ConfigProvider(
      load_function = {'function': get_constant_dict}, name = 'constant')
    self.assertNotEqual(
      config_provider.ConfigProvider.getInstance(),
      config_provider.ConfigProvider.getInstance(name = 'constant'))
    self.assertEqual(config_provider.ConfigProvider.getInstance(), self.config)